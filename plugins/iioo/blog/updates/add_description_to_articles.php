<?php
namespace IIOO\Catalogue\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddDescriptionToArticlesMigration extends Migration
{
    public function up()
    {
        Schema::table('iioo_blog_articles', function (Blueprint $table) {
            $table->text('description');
        });
    }

    public function down()
    {
        Schema::table('iioo_blog_articles', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }
}
?>