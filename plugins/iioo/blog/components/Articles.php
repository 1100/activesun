<?php namespace IIOO\Blog\Components;

use Cms\Classes\ComponentBase;
use IIOO\Blog\Models\Article;

class Articles extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'iioo.blog::lang.components.articles.name',
            'description' => 'iioo.blog::lang.components.articles.description'
        ];
    }

    public function onRun()
    {
        $this->checkCurrentArticle();
    }

    public function getArticles()
    {
        return Article::paginate(12);
    }

    private function checkCurrentArticle()
    {
        if ($this->param('article_slug')) {
            $article = Article::where('slug', $this->param('article_slug'))->first();
            if ($article) {
                $this->page['current_article'] = $article;
            } else {
                \Response::make($this->controller->run('404'), 404);
            }
        }
    }
}
