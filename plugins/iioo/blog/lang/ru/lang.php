<?php

return [
    'plugin' => [
        'name' => 'База знаний',
        'description' => 'Плагин IIOO.Blog',
    ],
    'navigation' => [
        'label' => 'База знаний',
        'sideMenu' => [
            'articles' => 'Статьи',
        ],
    ],
    'components' => [
        'articles' => [
            'name' => 'Статьи',
            'description' => 'Список статей для базы знаний',
        ],
    ],
    'columns' => [
        'article' => [
            'title' => 'Заголовок',
            'description' => 'Описание',
            'date' => 'Дата создания',
        ],
    ],
    'fields' => [
        'article' => [
            'title' => 'Заголовок',
            'slug' => 'Постоянная ссылка',
            'description' => 'Описание',
            'content' => 'Статья',
        ],
    ],
    'controller' => [
        'view' => [
            'articles' => [
                'new' => 'Новая статья',
                'breadcrumb_label' => 'Статьи',
                'creating' => 'Создание статьи',
                'return' => 'Вернуться к статьям',
                'delete_confirmation' => 'Вы действительно хотите удалить данную статью?',
            ],
        ],
        'list' => [
            'articles' => 'Статьи',
        ],
        'form' => [
            'articles' => [
                'name' => 'Статья',
                'create' => 'Добавление статьи',
                'update' => 'Изменение статьи',
                'flashCreate' => 'Статья добавлена',
                'flashUpdate' => 'Статья изменена',
                'flashDelete' => 'Статья удалена',
            ],
        ]
    ],
    'permissions' => [
        'tab' => 'База знаний',
        'manage' => 'Управление базой знаний'
    ],
];
