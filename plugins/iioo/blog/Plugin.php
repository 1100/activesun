<?php namespace IIOO\Blog;

use Backend;
use System\Classes\PluginBase;

/**
 * Blog Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'iioo.blog::lang.plugin.name',
            'description' => 'iioo.blog::lang.plugin.description',
            'author'      => 'Roman Movchan, Misha Cheypesh',
            'icon'        => 'icon-briefcase'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'IIOO\Blog\Components\Articles' => 'Articles',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'iioo.blog.access_blog' => [
                'tab'   => 'iioo.blog::lang.permissions.tab',
                'label' => 'iioo.blog::lang.permissions.manage',
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'blog' => [
                'permissions' => ['iioo.blog.access_blog'],
                'label' => 'iioo.blog::lang.navigation.label',
                'url' => Backend::url('iioo/blog/articles'),
                'icon' => 'icon-book',
                'order' => 500,
                'sideMenu' => [
                    'articles' => [
                        'permissions' => ['iioo.blog.access_blog'],
                        'label' => 'iioo.blog::lang.navigation.sideMenu.articles',
                        'url' => Backend::url('iioo/blog/articles'),
                        'icon' => 'icon-file-text-o',
                    ]
                ]
            ]
        ];
    }
}
