<?php namespace IIOO\Company\Components;

use Cms\Classes\ComponentBase;
use IIOO\Company\Models\Service;

class Services extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'iioo.company::lang.components.services.name',
            'description' => 'iioo.company::lang.components.services.description'
        ];
    }

    public function onRun()
    {
        $this->checkCurrentService();
    }

    public function getServices()
    {
        return Service::get();
    }

    private function checkCurrentService()
    {
        if ($this->param('service_slug')) {
            $service = Service::where('slug', $this->param('service_slug'))->first();
            if ($service) {
                $this->page['current_service'] = $service;
            } else {
                \Response::make($this->controller->run('404'), 404);
            }
        }
    }
}
