<?php namespace IIOO\Company\Components;

use Cms\Classes\ComponentBase;
use IIOO\Company\Models\Employee;

class Employees extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'iioo.company::lang.components.employees.name',
            'description' => 'iioo.company::lang.components.employees.description'
        ];
    }

    public function getEmployees()
    {
        return Employee::get();
    }
}
