<?php namespace IIOO\Company\Models;

use Model;

/**
 * Employee Model
 */
class Employee extends Model
{

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'iioo_company_employees';
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'image' => ['System\Models\File']
    ];
    public $attachMany = [];

    public $rules = [
        'first_name' => 'required',
        'last_name'  => 'required',
        'post'       => 'required',
        'image'      => 'required',
        'email'      => 'email',
    ];
}