<?php

return [
    'plugin' => [
        'name' => 'Компания',
        'description' => 'Плагин IIOO.Company',
    ],
    'components' => [
        'services' => [
            'name' => 'Услуги',
            'description' => 'Услуги на главную',
        ],
        'employees' => [
            'name' => 'Сотрудники',
            'description' => 'Блок сотрудников',
        ],
    ],
    'navigation' => [
        'label' => 'Компания',
        'sideMenu' => [
            'services' => 'Услуги',
            'employees' => 'Сотрудники',
        ],
    ],
    'columns' => [
        'service' => [
            'name' => 'Услуга',
            'description' => 'Краткое описание',
        ],
        'employee' => [
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'post' => 'Должность',
        ],
    ],
    'fields' => [
        'service' => [
            'name' => 'Услуга',
            'slug' => 'Постоянная ссылка',
            'description' => 'Краткое описание',
            'image' => 'Фоновое изображение блока',
            'content' => 'Содержимое страницы',
        ],
        'employee' => [
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'post' => 'Должность',
            'image' => 'Фотография',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'skype' => 'Skype',
        ],
    ],
    'controller' => [
        'view' => [
            'services' => [
                'new' => 'Новая услуга',
                'breadcrumb_label' => 'Услуги',
                'creating' => 'Создание услуги',
                'return' => 'Вернуться к услугам',
                'delete_confirmation' => 'Вы действительно хотите удалить данную услугу?',
            ],
            'employees' => [
                'new' => 'Новый сотрудник',
                'breadcrumb_label' => 'Сотрудники',
                'creating' => 'Создание сотрудника',
                'return' => 'Вернуться к сотрудникам',
                'delete_confirmation' => 'Вы действительно хотите удалить данного сотрудника?',
            ],
        ],
        'list' => [
            'services' => 'Услуги',
            'employees' => 'Сотрудники',
        ],
        'form' => [
            'services' => [
                'name' => 'Услуга',
                'create' => 'Добавление услуги',
                'update' => 'Изменение услуги',
                'flashCreate' => 'Услуга добавлена',
                'flashUpdate' => 'Услуга изменена',
                'flashDelete' => 'Услуга удалена',
            ],
            'employees' => [
                'name' => 'Сотрудник',
                'create' => 'Добавление сотрудника',
                'update' => 'Изменение сотрудника',
                'flashCreate' => 'Сотрудник добавлен',
                'flashUpdate' => 'Сотрудник изменен',
                'flashDelete' => 'Сотрудник удален',
            ],
        ]
    ],
    'permissions' => [
        'tab' => 'Компания',
        'manage' => 'Управление информацией о компании'
    ],
];
