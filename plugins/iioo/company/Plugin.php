<?php namespace IIOO\Company;

use Backend;
use System\Classes\PluginBase;

/**
 * Services Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'iioo.company::lang.plugin.name',
            'description' => 'iioo.company::lang.plugin.description',
            'author'      => 'Roman Movchan, Misha Cheypesh',
            'icon'        => 'icon-briefcase'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'IIOO\Company\Components\Services' => 'Services',
            'IIOO\Company\Components\Employees' => 'Employees',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'iioo.company.access_company' => [
                'tab'   => 'iioo.company::lang.permissions.tab',
                'label' => 'iioo.company::lang.permissions.manage',
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'company' => [
                'permissions' => ['iioo.company.access_company'],
                'label' => 'iioo.company::lang.navigation.label',
                'url' => Backend::url('iioo/company/services'),
                'icon' => 'icon-briefcase',
                'order' => 500,
                'sideMenu' => [
                    'services' => [
                        'permissions' => ['iioo.company.access_company'],
                        'label' => 'iioo.company::lang.navigation.sideMenu.services',
                        'url' => Backend::url('iioo/company/services'),
                        'icon' => 'icon-list-ol',
                    ],
                    'employees' => [
                        'permissions' => ['iioo.company.access_company'],
                        'label' => 'iioo.company::lang.navigation.sideMenu.employees',
                        'url' => Backend::url('iioo/company/employees'),
                        'icon' => 'icon-users',
                    ],
                ]
            ]
        ];
    }
}
