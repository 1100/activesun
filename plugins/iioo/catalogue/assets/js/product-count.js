function autoInputSize(input) {
    input.attr('size', Math.max(input.val().length, 1));
}

function bindCountHandlers() {
    $('.product-count').each(function() {
        var block = $(this);
        var input = block.find('input');
        var plus = block.find('.plus');
        var minus = block.find('.minus');

        plus.click(function () {
            autoInputSize(input);
            input.val(parseInt(input.val()) + 1);
            makeSendCountAjaxRequestTimeout(input);
        });
        minus.click(function () {
            if (input.val() > 1) {
                autoInputSize(input);
                input.val(parseInt(input.val()) - 1);
                makeSendCountAjaxRequestTimeout(input);
            }
        });
        input.keyup(autoInputSize);
        input.keydown(autoInputSize);
    });
}

function sendCountAjaxRequest(input) {
    input.request('Catalogue::onAddProductToCart',{
        data: {
            product_id: input.data('product-id'),
            products_count: input.val()
        },
        success: function(data) {
            $('#cart-header-block').html(data['#cart-header-block']);
            $('#cart-header-icon').html(data['#ccart-header-icon']);
            $('#cart-table').html(data['#cart-table']);
            bindCountHandlers();
        }
    })
}

var count_request_timeout;
function makeSendCountAjaxRequestTimeout(input) {
    if (input.hasClass('js-no-count-ajax')) {
        return;
    } else {
        if (typeof count_request_timeout !== 'undefined') {
            clearTimeout(count_request_timeout);
        }
        count_request_timeout = setTimeout(function () {
            sendCountAjaxRequest(input);
        }, 500);
    }
}

bindCountHandlers();