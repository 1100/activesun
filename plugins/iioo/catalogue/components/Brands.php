<?php
namespace IIOO\Catalogue\Components;

use Cms\Classes\ComponentBase;
use IIOO\Catalogue\Models\Brand;


class Brands extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'iioo.catalogue::lang.components.brands.name',
            'description' => 'iioo.catalogue::lang.components.brands.description',
        ];
    }

    public function getBrands()
    {
        return Brand::where('display_on_home', true)->get();
    }
}