<?php
namespace IIOO\Catalogue\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use October\Rain\Support\Facades\Flash;
use IIOO\Catalogue\Models\Category;
use IIOO\Catalogue\Models\Order;
use IIOO\Catalogue\Models\Product;
use IIOO\Catalogue\Models\Subcategory;
use IIOO\Catalogue\Models\Brand;
use IIOO\Catalogue\Models\Settings;
use Input;
use Mail;
use Cookie;
use Request;
use Response;
use Validator;
use AjaxException;

class Catalogue extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'iioo.catalogue::lang.components.catalogue.name',
            'description' => 'iioo.catalogue::lang.components.catalogue.description',
        ];
    }

    public $current_subcategory = null;
    public $is_public;
    public $cart = [];
    public $query = null;
    public $current_brands = null;
    public $min_price = null;
    public $max_price = null;
    public $order = null;
    public $comparison = [];
    private $comparison_products_count = null;
    public static $currency = null;

    public function onRun()
    {
        $this->addJs('/plugins/iioo/catalogue/assets/js/product-count.js');
        $this->is_public = ( Settings::get('public_status', '') ) ? true : false;
        $this->setSortedCartFromCookie();
        $this->checkCurrentSubcategory();
        $this->checkCurrentProduct();
        $this->checkCurrentFilter();
        $this->addCartProductsToPage();
        $this->addBrandsToPage();
        $this->setComparisonFromCookie();
        $this->addComparisonToPage();
    }

    public function init()
    {
        $this->setCurrencyFromCookie();
    }

    private function checkCurrentSubcategory()
    {
        if ($this->param('subcategory_slug')) {
            $this->current_subcategory = Subcategory::where('slug', $this->param('subcategory_slug'))->first();
            if (is_null($this->current_subcategory)) {
                \Response::make($this->controller->run('404'), 404);
            }
        }
    }

    private function checkCurrentFilter() {
        if (!Input::has('req') || (Input::has('req') && Input::get('req') != 'reset')) {
            if (Input::has('brands')) {
                $this->current_brands = Input::get('brands');
            }
            if (Input::has('query')) {
                $this->query = Input::get('query');
            }
            if (Input::has('min-price')) {
                $this->min_price = Input::get('min-price');
            }
            if (Input::has('max-price')) {
                $this->max_price = Input::get('max-price');
            }
            if (Input::has('currency')) {
                Cookie::queue('currency', Input::get('currency'));
                self::$currency = Input::get('currency');
            }
            if (Input::has('order')) {
                $this->order = Input::get('order');
            }
        }
    }

    private function checkCurrentProduct()
    {
        if ($this->param('product_slug')) {
            $product = Product::where('slug', $this->param('product_slug'))->with('subcategory')->first();
            if ($product) {
                $this->page['product'] = $product;
            } else {
                \Response::make($this->controller->run('404'), 404);
            }
        }
    }

    public function getSubcategory()
    {
        return $this->current_subcategory;
    }

    public function getSubcategorySlug()
    {
        return $this->current_subcategory ? $this->current_subcategory->slug : null;
    }

    public function getSubcategoryName()
    {
        return $this->current_subcategory ? $this->current_subcategory->name : null;
    }

    public function getCategories()
    {
        return Category::with('subcategories')->get();
    }

    public function getProducts()
    {
        if ($this->current_subcategory) {
            $products =  $this->current_subcategory->products();
        } else {
            $products =  Product::with('subcategory');
        }
        $products->orderBy('is_available', 'desc');
        if ($this->current_brands) {
            $products = $products->whereIn('brand_id', $this->current_brands);
        }
        if ($this->query) {
            $products = $products->where('name', 'like', '%' . $this->query . '%')->orWhere('keywords', 'like', '%' . $this->query . '%');
        }
        if ($this->order === 'min_price_first') {
            $products->orderBy('price', 'asc');
        }
        if ($this->order === 'max_price_first') {
            $products->orderBy('price', 'desc');
        }

        $products = $products->get();

        if (!is_null($this->min_price)) {
            $products = $products->filter(function ($item) {
                return $item['currency_price'] >= $this->min_price;
            });
        }
        if (!is_null($this->max_price)) {
            $products = $products->filter(function ($item) {
                return $item['currency_price'] <= $this->max_price;
            });
        }

        return $this->paginate($products);
    }

    public function paginate($items, $perPage = 12, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $options['path'] = $this->currentPageUrl();
        $options['query'] = $_GET;
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function onSelectSubcategory()
    {
        if (post('subcategory_slug')) {
            $this->current_subcategory = Subcategory::where('slug', post('subcategory_slug'))->first();
        }
        return ['#products' => $this->renderPartial('@products.htm'),
                '#categories' => $this->renderPartial('@categories.htm')];
    }


    public function onAddProductToCart()
    {
        $this->setSortedCartFromCookie();
        if (post('products_count')) {
            if (post('products_count') > 0) {
                $this->cart[post('product_id')]= [
                    'count' => post('products_count'),
                    'adding_time' => (isset($this->cart[post('product_id')]) && !empty($this->cart[post('product_id')])) ? $this->cart[post('product_id')]['adding_time'] : time(),
                ];
            } else {
                unset($this->cart[post('product_id')]);
            }
        } else {
            $this->cart[post('product_id')] = [
                'count' => 1,
                'adding_time' => time(),
            ];
        }
        $this->addCartProductsToPage();
        return Response::json([
            '#cart-header-block' => $this->renderPartial('@cart-header-block.htm'),
            '#cart-header-icon' => $this->renderPartial('@cart-header-icon.htm'),
            '#cart-table' => $this->renderPartial('@cart-table.htm'),
            '#add-to-cart-btn-'.post('product_id') => $this->renderPartial('@add-to-cart-btn.htm', [
                'product_id' => post('product_id'),
                'in_cart' => true,
            ])
        ])->withCookie('cart', $this->cart);
    }

    public function onRemoveProductFromCart()
    {
        $this->setSortedCartFromCookie();
        unset($this->cart[post('product_id')]);
        $this->addCartProductsToPage();
        return Response::json([
            '#cart-header-block' => $this->renderPartial('@cart-header-block.htm'),
            '#cart-header-icon' => $this->renderPartial('@cart-header-icon.htm'),
            '#cart-table' => $this->renderPartial('@cart-table.htm'),
            '#add-to-cart-btn-'.post('product_id') => $this->renderPartial('@add-to-cart-btn.htm', [
                'product_id' => post('product_id'),
                'in_cart' => false,
            ]),
            '#checkout-information' => $this->renderPartial('@checkout-information')
        ])->withCookie('cart', $this->cart);
    }

    public function onAddProductToComparison()
    {
        $this->setComparisonFromCookie();
        if (!$this->isProductIdInComparison(post('product_id'))) {
            $product = Product::findOrFail(post('product_id'));
            $this->comparison[$product->subcategory->id]['id'] = $product->subcategory->id;
            $this->comparison[$product->subcategory->id]['slug'] = $product->subcategory->slug;
            $this->comparison[$product->subcategory->id]['name'] = $product->subcategory->name;
            $this->comparison[$product->subcategory->id]['products'][] = $product->id;
            return Response::json([
                '#comparison-header-icon' => $this->renderPartial('@comparison-header-icon.htm', [
                    'comparison_products_count' => $this->getComparisonProductsCount(),
                ]),
                '#comparison-header-block' => $this->renderPartial('@comparison-header-block.htm', [
                    'comparison_products_count' => $this->getComparisonProductsCount(),
                    'comparison' => $this->comparison,
                ]),
                '#add-to-comparison-btn-' . $product->id => $this->renderPartial('@add-to-comparison-btn.htm', [
                    'product_id' => $product->id,
                    'in_comparison' => true,
                ]),
            ])->withCookie('comparison', $this->comparison);
        } else {
            return Response::make('product not found', 404);
        }
    }

    public function onRemoveProductFromComparison()
    {
        $this->setComparisonFromCookie();
        $product_index = array_search(post('product_id'), $this->comparison[post('subcategory_id')]['products']);
        unset($this->comparison[post('subcategory_id')]['products'][$product_index]);
        $response_content = [
            '#comparison-header-icon' => $this->renderPartial('@comparison-header-icon.htm', [
                'comparison_products_count' => $this->getComparisonProductsCount(),
            ]),
            '#comparison-header-block' => $this->renderPartial('@comparison-header-block.htm', [
                'comparison_products_count' => $this->getComparisonProductsCount(),
                'comparison' => $this->comparison
            ]),
            '#subcategory-tab-' . post('subcategory_id') => $this->renderPartial('@comparison-table.htm', [
                'comparison' => $this->comparison,
                'subcategory_id' => post('subcategory_id'),
            ]),
        ];
        return Response::json($response_content)->withCookie('comparison', $this->comparison);
    }

    public function onRemoveSubcategoryFromComparison()
    {
        $this->setComparisonFromCookie();
        $products = Product::find($this->comparison[post('subcategory_id')]['products']);
        unset($this->comparison[post('subcategory_id')]);
        $response_content = [
            '#comparison-header-icon' => $this->renderPartial('@comparison-header-icon.htm', [
                'comparison_products_count' => $this->getComparisonProductsCount(),
            ]),
            '#comparison-header-block' => $this->renderPartial('@comparison-header-block.htm', [
                'comparison_products_count' => $this->getComparisonProductsCount(),
                'comparison' => $this->comparison
            ]),
            '#subcategory-tab-' . post('subcategory_id') => $this->renderPartial('@comparison-table.htm', [
                'comparison' => $this->comparison,
                'subcategory_id' => post('subcategory_id'),
            ]),
        ];
        foreach ($products as $product) {
            $response_content['#add-to-comparison-btn-' . $product->id] = $this->renderPartial('@add-to-comparison-btn.htm', [
                'product_id' => $product->id,
                'in_comparison' => false
            ]);
        }
        return Response::json($response_content)->withCookie('comparison', $this->comparison);
    }

    public function onSubmitCheckout()
    {
        $customValidationMessages = [
            'first-name.required' => trans('iioo.catalogue::lang.components.validation.first_name.required'),
            'last-name.required' => trans('iioo.catalogue::lang.components.validation.last_name.required'),
            'phone.required' => trans('iioo.catalogue::lang.components.validation.phone.required'),
            'email.email' => trans('iioo.catalogue::lang.components.validation.email.email'),
        ];
        $formValidationRules = [
            'first-name' => 'required|string',
            'last-name' => 'required|string',
            'phone' => 'required|regex:/([0-9 \-+().])*/',
            'email' => 'email',
        ];

        $validator = Validator::make(post(), $formValidationRules, $customValidationMessages);

        $this->setSortedCartFromCookie();

        if ($validator->fails() || empty($this->cart)) {

            $error_messages = $validator->messages();
            Flash::error($error_messages->first());

            throw new AjaxException([
                '#checkout_form' => $this->renderPartial('@flash-message.htm', [
                    'errors' => $error_messages->all()
                ])
            ]);

        } else {

            $order = Order::create([
                'first_name' => post('first-name'),
                'last_name' => post('last-name'),
                'phone' => post('phone'),
                'email' => post('email'),
            ]);
            $selected_products = $this->getSelectedProducts()->pluck('id');
            $products = [];
            foreach ($selected_products as $id) {
                $products[$id] = ['products_count' => $this->cart[$id]['count']];
            }

            $order->products()->attach($products);

            if( !empty(Settings::get('notification_email_address', '')) ) {
                $this->sendNotificationMail($order);
            }

            Flash::success(trans('iioo.contact::lang.components.flash.success_checkout'));
            $this->cart = null;
            $this->getProducts();

            return Response::json([
                '#cart-header-block' => $this->renderPartial('@cart-header-block.htm'),
                '#cart-header-icon' => $this->renderPartial('@cart-header-icon.htm'),
                '#checkout-form' => $this->renderPartial('@submit-checkout-success.htm'),
            ])->withCookie(Cookie::forget('cart'));
        }
    }

    protected function sendNotificationMail($order)
    {
        $price = 0;
        foreach ($order->products as $product) {
            $price += $product->currency_price * $product->pivot->products_count;
        }
        $vars = [
            'customer_first_name' => $order->first_name,
            'customer_last_name' => $order->last_name,
            'customer_phone' => $order->phone,
            'customer_email' => $order->email,
            'products' => $order->products,
            'price' => $price,
            'currency_symbol' => self::getCurrencySymbol(),
        ];

        $recipients = array_map('trim', explode(',', Settings::get('notification_email_address')));

        Mail::sendTo($recipients, 'iioo.catalogue::mail.notification_message', $vars);
    }

    public function getSelectedProducts() {
        if ($this->cart) {
            return Product::find(array_keys($this->cart))->sort(function ($a_product, $b_product) {
                return $this->cart[$b_product->id]['adding_time'] - $this->cart[$a_product->id]['adding_time'];
            });
        }
    }

    private function addCartProductsToPage()
    {
        if ($this->cart) {
            $products = $this->getSelectedProducts();
            $price = 0;
            $all_products_count = 0;
            foreach ($products as $product) {
                $price += $product->currency_price * $this->cart[$product->id]['count'];
                $all_products_count += $this->cart[$product->id]['count'];
            }
            $this->page['cart'] = $this->cart;
            $this->page['cart_price'] = $price;
            $this->page['products'] = $products;
            $this->page['products_count'] = $products->count();
            $this->page['all_products_count'] = $all_products_count;
        } else {
            $this->page['cart'] = null;
            $this->page['cart_price'] = 0;
            $this->page['products'] = null;
            $this->page['products_count'] = 0;
            $this->page['all_products_count'] = 0;
        }
    }

    private function addComparisonToPage()
    {
        if ($this->comparison) {
            $subcategories = Subcategory::find(array_column($this->comparison, 'id'));
            $this->page['comparison'] = $this->comparison;
            $this->page['comparison_products_count'] = $this->getComparisonProductsCount();
            $this->page['comparison_subcategories'] = $subcategories;
        }
    }

    private function getComparisonProductsCount()
    {
        if (is_null($this->comparison_products_count)) {
            foreach ($this->comparison as $subcategory) {
                $this->comparison_products_count += count($subcategory['products']);
            }
        }
        return $this->comparison_products_count;
    }

    private function addBrandsToPage()
    {
        $this->page['brands'] = Brand::all();
    }

    private function setSortedCartFromCookie()
    {
        $this->cart = Request::cookie('cart') ? Request::cookie('cart') : [];
        uasort($this->cart, function ($a_product, $b_product) {
            return $a_product['adding_time'] - $b_product['adding_time'];
        });
    }

    private function setComparisonFromCookie()
    {
        $this->comparison = Request::cookie('comparison') ? Request::cookie('comparison') : [];
    }

    public function isProductIdInComparison($product_id)
    {
        foreach ($this->comparison as $subcategory) {
            if (in_array($product_id, $subcategory['products'])) {
                return true;
            }
        }
    }

    public function getComparisonFormattedDataForSubcategory($subcategory_id)
    {
        $products = Product::with('information')->find($this->comparison[$subcategory_id]['products']);
        $data = [];
        $data['product_slug']['label'] = 'Постоянная ссылка продукта';
        $data['main_image']['label'] = 'Фото';
        $data['name']['label'] = 'Название';
        $data['price']['label'] = 'Цена';
        $data['brand']['label'] = 'Производитель';
        $data['description']['label'] = 'Описание';
        foreach ($products as $product) {
            $data['product_slug']['values'][$product->id] = $product->slug;
            $data['main_image']['values'][$product->id] = $product->main_image;
            $data['name']['values'][$product->id] = $product->name;
            $data['price']['values'][$product->id] = self::getCurrencySymbol() . ' ' . $product->currency_price;
            $data['brand']['values'][$product->id] = $product->brand->name;
            $data['description']['values'][$product->id] = $product->description;
        }
        $subcategory = Subcategory::find($subcategory_id);
        $data['subcategory_slug']['label'] = 'Постоянная ссылка подкатегории';
        $data['subcategory_slug']['value'] = $subcategory->slug;
        $subcategory_information = $subcategory->information()->orderBy('position', 'desc')->get();
        foreach ($subcategory_information as $information) {
            $data['information'][$information->id]['label'] = $information->name;
            foreach ($products as $product) {
                $data['information'][$information->id]['values'][$product->id] = $product->getInformationValue($information->id);
            }
        }
        return $data;
    }

    public static function getCurrencyOptions()
    {
        return [
            'USD' => '$ USD',
            'UAH' => '₴ UAH',
            'EUR' => '€ EUR'
        ];
    }

    public static function getCurrencySymbol($currency = null)
    {
        if (is_null($currency)) {
            $currency = self::$currency;
        }
        $currency_symbols = [
            'USD' => '$',
            'UAH' => '₴',
            'EUR' => '€'
        ];
        if (array_key_exists($currency, $currency_symbols)) {
            return $currency_symbols[$currency];
        } else {
            return $currency;
        }
    }

    public function setCurrencyFromCookie()
    {
        self::$currency = Request::cookie('currency') ? Request::cookie('currency') : 'USD';
    }

    public function getCurrency()
    {
        return self::$currency;
    }

}