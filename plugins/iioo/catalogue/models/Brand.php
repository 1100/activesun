<?php
namespace IIOO\Catalogue\Models;

use Model;

/**
 * Item Model
 */
class Brand extends Model
{

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'iioo_ctlg_brands';
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'products' => [
            'IIOO\Catalogue\Models\Product',
            'key' => 'brand_id',
        ],
        'products_count' => [
            'IIOO\Catalogue\Models\Product',
            'key' => 'brand_id',
            'count' => true,
        ],
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'image' => ['System\Models\File']
    ];
    public $attachMany = [];

    public $rules = [
        'slug'                  => 'required|unique:iioo_ctlg_brands,slug',
    ];
}