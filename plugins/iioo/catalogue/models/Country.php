<?php
namespace IIOO\Catalogue\Models;

use Model;

/**
 * Item Model
 */
class Country extends Model
{

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'iioo_ctlg_countries';
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'products' => [
            'IIOO\Catalogue\Models\Product',
            'key' => 'country_id',
            'other_key' => 'id'
        ],
        'products_count' => [
            'IIOO\Catalogue\Models\Product',
            'key' => 'country_id',
            'other_key' => 'id',
            'count' => true,
        ],
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public $rules = [
        'name'            => 'required|unique:iioo_ctlg_countries,name',
        'code'            => 'required|unique:iioo_ctlg_countries,code',
    ];
}