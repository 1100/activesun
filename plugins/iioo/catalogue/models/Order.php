<?php
namespace IIOO\Catalogue\Models;

use Model;

/**
 * Item Model
 */
class Order extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'iioo_ctlg_orders';
    public $timestamps = true;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['first_name', 'last_name', 'phone', 'email'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'products' => [
            'IIOO\Catalogue\Models\Product',
            'table' => 'iioo_ctlg_order_product',
            'key' => 'order_id',
            'other_key' => 'product_id',
            'pivot' => [
                'products_count'
            ],
        ],
        'products_count' => [
            'IIOO\Catalogue\Models\Product',
            'table' => 'iioo_ctlg_order_product',
            'key' => 'order_id',
            'other_key' => 'product_id',
            'count' => true,
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function addProducts($products)
    {
        return null;
    }
}