<?php
namespace IIOO\Catalogue\Models;

use Model;

/**
 * Item Model
 */
class InformationField extends Model
{

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'iioo_ctlg_information_fields';
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'subcategories' => [
            'IIOO\Catalogue\Models\Subcategory',
            'table' => 'iioo_ctlg_subcategory_information',
            'key' => 'information_field_id',
            'other_key' => 'subcategory_id',
            'pivot' => [
                'position',
            ]
        ],
        'products' => [
            'IIOO\Catalogue\Models\Product',
            'table' => 'iioo_ctlg_product_information',
            'key' => 'information_field_id',
            'other_key' => 'product_id',
            'pivot' => [
                'value'
            ],
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public $rules = [
        'name'                  => 'required|unique:iioo_ctlg_information_fields,name',
    ];
}