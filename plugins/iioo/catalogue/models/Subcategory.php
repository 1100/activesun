<?php
namespace IIOO\Catalogue\Models;

use Model;

/**
 * Item Model
 */
class Subcategory extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'iioo_ctlg_subcategories';
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'products' => [
            'IIOO\Catalogue\Models\Product',
            'key' => 'subcategory_id',
        ],
        'products_count' => [
            'IIOO\Catalogue\Models\Product',
            'key' => 'subcategory_id',
            'count' => true,
        ]
    ];
    public $belongsTo = [
        'category' => [
            'IIOO\Catalogue\Models\Category',
            'key' => 'category_id',
        ],
    ];
    public $belongsToMany = [
        'information' => [
            'IIOO\Catalogue\Models\InformationField',
            'table' => 'iioo_ctlg_subcategory_information',
            'key' => 'subcategory_id',
            'other_key' => 'information_field_id',
            'pivot' => [
                'position',
            ],
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public $rules = [
        'slug'                 => 'required|unique:iioo_ctlg_subcategories,slug',
        'category_id'          => 'required|exists:iioo_ctlg_categories,id',
    ];
}