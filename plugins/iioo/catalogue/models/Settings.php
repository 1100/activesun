<?php namespace IIOO\Catalogue\Models;

use Model;

/**
 * Settings Model
 */
class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];
    public $settingsCode = 'iioo_ctlg_settings';
    public $settingsFields = 'fields.yaml';
}
