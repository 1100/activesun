<?php
namespace IIOO\Catalogue\Models;

use Model;

/**
 * Item Model
 */
class Product extends Model
{

    use \October\Rain\Database\Traits\Validation;

    public static $currency_data = [];
    public static $currency_code_keys = [];
    public static $currency_rates = [];
    public $currency_prices = [];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'iioo_ctlg_products';
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'brand' => [
            'IIOO\Catalogue\Models\Brand',
            'key' => 'brand_id',
        ],
        'subcategory' => [
            'IIOO\Catalogue\Models\Subcategory',
            'key' => 'subcategory_id',
        ],
        'country' => [
            'IIOO\Catalogue\Models\Country',
            'key' => 'country_id',
            'otherKey' => 'id',
        ]
    ];
    public $belongsToMany = [
        'information' => [
            'IIOO\Catalogue\Models\InformationField',
            'table' => 'iioo_ctlg_product_information',
            'key' => 'product_id',
            'other_key' => 'information_field_id',
            'pivot' => [
                'value'
            ]
        ],
        'orders' => [
            'IIOO\Catalogue\Models\Order',
            'table' => 'iioo_ctlg_order_product',
            'key' => 'product_id',
            'other_key' => 'order_id',
            'pivot' => [
                'products_count',
            ]
        ],
        'orders_count' => [
            'IIOO\Catalogue\Models\Order',
            'table' => 'iioo_ctlg_order_product',
            'key' => 'product_id',
            'other_key' => 'order_id',
            'count' => true,
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'main_image' => ['System\Models\File']
    ];
    public $attachMany = [
        'images' => ['System\Models\File']
    ];

    public function getInformationValue($field_id)
    {
        $field = $this->information()->where('information_field_id', $field_id)->first();
        if ($field) {
            return $field->pivot->value;
        } else {
            return null;
        }
    }

    public $rules = [
        'name'            => 'required',
        'slug'            => 'required|unique:iioo_ctlg_products,slug',
        'subcategory_id'  => 'required|exists:iioo_ctlg_subcategories,id',
        'brand_id'        => 'required|exists:iioo_ctlg_brands,id',
        'price'           => 'required|numeric|min:0',
        'currency'        => 'required',
        'code'            => 'required|unique:iioo_ctlg_products,slug',
    ];

    public function getCurrencyOptions()
    {
        return \IIOO\Catalogue\Components\Catalogue::getCurrencyOptions();
    }

    public function getCurrencyData()
    {
        if(empty(self::$currency_data)) {
            $url = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=11';
            self::$currency_data = json_decode(file_get_contents($url), true);
        }
        return self::$currency_data;
    }

    public function getCurrencyCodeKey($currency_code)
    {
        if(empty(self::$currency_data)) {
            self::$currency_code_keys = array_column($this->getCurrencyData(), 'ccy');
        }
        return array_search($currency_code, self::$currency_code_keys);
    }

    public function getCurrencyRate($currency_code)
    {
        if(! array_key_exists($currency_code, self::$currency_rates)) {
            if ($currency_code === 'UAH') {
                self::$currency_rates[$currency_code] = 1;
            } else {
                $key = $this->getCurrencyCodeKey($currency_code);
                self::$currency_rates[$currency_code] = $this->getCurrencyData()[$key]['sale'];
            }
        }
        return self::$currency_rates[$currency_code];
    }

    public function getPriceInCurrency($currency_code)
    {
        if(! array_key_exists($currency_code, $this->currency_prices)) {
            $from_rate = $this->getCurrencyRate($this->currency);
            $to_rate = $this->getCurrencyRate($currency_code);
            $this->currency_prices[$currency_code] = round(($this->price*$from_rate)/$to_rate, 2);
        }
        return $this->currency_prices[$currency_code];
    }

    public function getCurrencyPriceAttribute($value)
    {
        return $this->getPriceInCurrency(\IIOO\Catalogue\Components\Catalogue::$currency);
    }

}