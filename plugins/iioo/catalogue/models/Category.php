<?php
namespace IIOO\Catalogue\Models;

use Model;

/**
 * Item Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Validation;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'iioo_ctlg_categories';
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'subcategories' => [
            'IIOO\Catalogue\Models\Subcategory',
            'key' => 'category_id',
            'other_key' => 'id',
        ],
        'subcategories_count' => [
            'IIOO\Catalogue\Models\Subcategory',
            'key' => 'category_id',
            'other_key' => 'id',
            'count' => true,
        ],
    ];
    public $hasManyThrough = [
        'products' => [
            'IIOO\Catalogue\Models\Product',
            'key' => 'category_id',
            'through' => 'IIOO\Catalogue\Models\Subcategory',
            'throughKey' => 'subcategory_id',
        ],
        'products_count' => [
            'IIOO\Catalogue\Models\Product',
            'key' => 'category_id',
            'through' => 'IIOO\Catalogue\Models\Subcategory',
            'throughKey' => 'subcategory_id',
            'count' => true,
        ]
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public $rules = [
        'slug'                  => 'required|unique:iioo_ctlg_categories,slug',
        'name'                 => 'required|unique:iioo_ctlg_categories,slug',
    ];
}