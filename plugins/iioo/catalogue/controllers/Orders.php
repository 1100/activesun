<?php
namespace IIOO\Catalogue\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use IIOO\Catalogue\Models\Order;

/**
 * Items Back-end Controller
 */
class Orders extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';


    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('IIOO.Catalogue', 'catalogue', 'orders');
    }

    public function getCurrenciesPrice()
    {
        $order = Order::find($this->params[0]);
        $currencies = \IIOO\Catalogue\Components\Catalogue::getCurrencyOptions();
        $currencies_price = [];
        foreach ($order->products as $product) {
            foreach ($currencies as $currency_code => $currency_text) {
                $product_price = $product->getPriceInCurrency($currency_code);
                if (!array_key_exists($currency_code, $currencies_price)) {
                    $currencies_price[$currency_code] = 0;
                }
                $currencies_price[$currency_code] += $product_price * $product->pivot->products_count;
            }
        }
        $prices = [];
        foreach ($currencies as $currency_code => $currency_text) {
            $price = '';
            $price .= \IIOO\Catalogue\Components\Catalogue::getCurrencySymbol($currency_code);
            $price .= ' '.$currencies_price[$currency_code];
            $prices[] = $price;
        }
        return implode(' / ', $prices);
    }
}