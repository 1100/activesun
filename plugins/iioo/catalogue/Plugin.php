<?php
namespace IIOO\Catalogue;

use Backend;
use System\Classes\PluginBase;

/**
 * catalogue Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'iioo.catalogue::lang.plugin.name',
            'description' => 'iioo.catalogue::lang.plugin.description',
            'author'      => 'Roman Movchan, Misha Cheypesh',
            'icon'        => 'icon-cubes'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'IIOO\Catalogue\Components\Catalogue' => 'Catalogue',
            'IIOO\Catalogue\Components\Brands' => 'Brands',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'iioo.catalogue.access_catalogue' => [
                'tab'   => 'iioo.catalogue::lang.permissions.tab',
                'label' => 'iioo.catalogue::lang.permissions.manage',
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'catalogue' => [
                'permissions' => ['iioo.catalogue.access_catalogue'],
                'label' => 'iioo.catalogue::lang.navigation.label',
                'url' => Backend::url('iioo/catalogue/orders'),
                'icon' => 'icon-cubes',
                'order' => 500,
                'sideMenu' => [
                    'orders' => [
                        'permissions' => ['iioo.catalogue.access_catalogue'],
                        'label' => 'iioo.catalogue::lang.navigation.sideMenu.orders',
                        'url' => Backend::url('iioo/catalogue/orders'),
                        'icon' => 'icon-cart-arrow-down',
                    ],
                    'products' => [
                        'permissions' => ['iioo.catalogue.access_catalogue'],
                        'label' => 'iioo.catalogue::lang.navigation.sideMenu.products',
                        'url' => Backend::url('iioo/catalogue/products'),
                        'icon' => 'icon-cube',
                    ],
                    'brands' => [
                        'permissions' => ['iioo.catalogue.access_catalogue'],
                        'label' => 'iioo.catalogue::lang.navigation.sideMenu.brands',
                        'url' => Backend::url('iioo/catalogue/brands'),
                        'icon' => 'icon-industry',
                    ],
                    'categories' => [
                        'permissions' => ['iioo.catalogue.access_catalogue'],
                        'label' => 'iioo.catalogue::lang.navigation.sideMenu.categories',
                        'url' => Backend::url('iioo/catalogue/categories'),
                        'icon' => 'icon-sitemap',
                    ],
                    'subcategories' => [
                        'permissions' => ['iioo.catalogue.access_catalogue'],
                        'label' => 'iioo.catalogue::lang.navigation.sideMenu.subcategories',
                        'url' => Backend::url('iioo/catalogue/subcategories'),
                        'icon' => 'icon-sitemap',
                    ],
                    'information_fields' => [
                        'permissions' => ['iioo.catalogue.access_catalogue'],
                        'label' => 'iioo.catalogue::lang.navigation.sideMenu.information_fields',
                        'url' => Backend::url('iioo/catalogue/informationFields'),
                        'icon' => 'icon-list-ol',
                    ]
                ]
            ]
        ];
    }

    /**
     * Registers back-end settings for this plugin.
     *
     * @return array
     */
    public function registerSettings()
    {
        return [
            'config' => [
                'label'       => 'iioo.catalogue::lang.settings.name',
                'icon'        => 'icon-cubes',
                'description' => 'iioo.catalogue::lang.settings.description',
                'class'       => 'IIOO\Catalogue\Models\Settings',
                'permissions' => ['iioo.catalogue.access_catalogue'],
                'order'       => 60
            ]
        ];
    }
}
