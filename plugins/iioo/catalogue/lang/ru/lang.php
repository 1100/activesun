<?php

return [
    'plugin' => [
        'name' => 'Каталог',
        'description' => 'Плагин IIOO.Catalogue',
    ],
    'settings' => [
        'name' => 'Каталог',
        'description' => 'Настройки каталога',
        'public_status' => 'Открытый',
        'public_status_comment' => 'Отображать каталог на сайте',
        'notification_email_address' => 'Email для уведомлений',
        'notification_email_address_comment' => 'Можно несколько через запятую',
    ],
    'components' => [
        'brands' => [
            'name' => 'Бренды',
            'description' => 'Бренды на главную',
        ],
        'catalogue' => [
            'name' => 'Каталог',
            'description' => 'Каталог продуктов',
        ],
        'currency_select' => [
            'name' => 'Выбор валюты',
            'description' => 'Выбор валюты для каталога',
        ],
        'validation' => [
            'first_name' => [
                'required' => 'Поле "Имя" обязательно',
            ],
            'last_name' => [
                'required' => 'Поле "Фамилия" обязательно',
            ],
            'phone' => [
                'required' => 'Поле "Телефон" обязательно',
            ],
            'email' => [
                'email' => 'Поле "Email" должно быть почтовым адресом',
            ],
        ],
        'flash' => [
            'success_checkout' => 'Спасибо, Ваш заказ пинят. В ближайшее время с вами свяжутся'
        ],
    ],
    'navigation' => [
        'label' => 'Каталог',
        'sideMenu' => [
            'orders' => 'Заказы',
            'products' => 'Продукция',
            'brands' => 'Производители',
            'categories' => 'Категории',
            'subcategories' => 'Подкатегории',
            'information_fields' => 'Свойства продукции',
        ],
    ],
    'columns' => [
        'category' => [
            'name' => 'Имя',
            'subcategories_count' => 'Кол-во подкатегорий',
            'products_count' => 'Кол-во продукции',
        ],
        'subcategory' => [
            'name' => 'Имя',
            'category' => 'Категория',
            'products_count' => 'Кол. продукции'
        ],
        'brand' => [
            'name' => 'Производитель',
            'display_on_home' => 'На домашней стр.',
            'products_count' => 'Количество продукции',
        ],
        'information_field' => [
            'name' => 'Имя',
        ],
        'product' => [
            'name' => 'Название',
            'category' => 'Категория',
            'subcategory' => 'Подкатегория',
            'brand' => 'Производитель',
            'country' => 'Страна',
            'price' => 'Цена',
            'is_available' => 'В наличии',
            'code' => 'Код',
            'keywords' => 'Ключевые слова'
        ],
        'order' => [
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'phone' => 'Телефон',
            'email' => 'Email',
            'created_at' => 'Дата оформления',
        ]
    ],
    'fields' => [
        'category' => [
            'name' => 'Имя',
            'slug' => 'Постоянная ссылка',
        ],
        'subcategory' => [
            'name' => 'Имя',
            'slug' => 'Постоянная ссылка',
            'category' => 'Категория',
            'information' => 'Свойства продуктов подкатегории',
            'the_information' => 'свойства'
        ],
        'brand' => [
            'name' => 'Имя',
            'slug' => 'Постоянная ссылка',
            'display_on_home' => 'Показывать на домашней странице',
            'image' => 'Логотип',
        ],
        'information_field' => [
            'name' => 'Имя'
        ],
        'product' => [
            'name' => 'Название',
            'slug' => 'Постоянная ссылка',
            'description' => 'Описание',
            'category' => 'Категория',
            'subcategory' => 'Подкатегория',
            'brand' => 'Производитель',
            'country' => 'Страна',
            'price' => 'Цена',
            'currency' => 'Валюта',
            'information' => 'Свойства',
            'the_information' => 'cвойства',
            'information_name' => 'Название',
            'information_value' => 'Значение свойства',
            'main_image' => 'Титульное изображение',
            'images' => 'Изображения',
            'is_available' => 'Есть в наличии',
            'code' => 'Код товара',
            'keywords' => 'Ключевые слова'
        ],
        'order' => [
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'phone' => 'Телефон',
            'email' => 'Email',
            'created_at' => 'Дата оформления',
            'products' => 'Продукты',
            'the_products' => 'продукта',
            'products_name' => 'Название',
            'products_price' => 'Цена',
            'products_currency' => 'Валюта',
            'products_count' => 'Количество',
            'price' => 'Общая стоимость',
        ],
    ],
    'controller' => [
        'view' => [
            'orders' => [
                'breadcrumb_label' => 'Заказы',
                'preview' => 'Просмотреть',
            ],
            'products' => [
                'new' => 'Новый продукт',
                'breadcrumb_label' => 'Продукты',
            ],
            'brands' => [
                'new' => 'Новый производитель',
                'breadcrumb_label' => 'Производители',
            ],
            'categories' => [
                'new' => 'Новая категория',
                'breadcrumb_label' => 'Категории',
            ],
            'subcategories' => [
                'new' => 'Новая подкатегория',
                'breadcrumb_label' => 'Подкатегории',
            ],
            'information_fields' => [
                'new' => 'Новое свойство',
                'breadcrumb_label' => 'Свойства',
            ],
        ],
        'list' => [
            'orders' => 'Заказы',
            'products' => 'Продукты',
            'brands' => 'Производители',
            'categories' => 'Категории',
            'subcategories' => 'Подкатегории',
            'information_fields' => 'Свойства продукции',
        ],
        'form' => [
            'orders' => [
                'preview' => 'Просмотр'
            ],
            'products' => [
                'create' => 'Добавление продукта',
                'update' => 'Изменение продукта',
                'flashCreate' => 'Продукт добавлен',
                'flashUpdate' => 'Продукт изменён',
                'flashDelete' => 'Продукт удалён',
            ],
            'brand' => [
                'create' => 'Добавление производителя',
                'update' => 'Изменение производителя',
                'flashCreate' => 'Прозводитель добавлен',
                'flashUpdate' => 'Производитель изменён',
                'flashDelete' => 'Производитель удалён',
            ],
            'category' => [
                'create' => 'Добавление категории',
                'update' => 'Изменение категории',
                'flashCreate' => 'Категория добавлена',
                'flashUpdate' => 'Категория изменена',
                'flashDelete' => 'категория удалена',
            ],
            'subcategory' => [
                'create' => 'Добавление подкатегории',
                'update' => 'Изменение подкатегории',
                'flashCreate' => 'Подкатегория добавлена',
                'flashUpdate' => 'Подкатегория изменена',
                'flashDelete' => 'Подкатегория удалена',
            ],
            'information_field' => [
                'create' => 'Добавление свойства',
                'update' => 'Изменение свойства',
                'flashCreate' => 'Свойство добавлено',
                'flashUpdate' => 'Свойство изменено',
                'flashDelete' => 'Свойство удалено',
            ],
        ]
    ],
    'permissions' => [
        'tab' => 'Каталог',
        'manage' => 'Управление каталогом'
    ],
];
