<?php
namespace IIOO\Catalogue\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class InitialMigration extends Migration
{
    public function up()
    {
        Schema::create('iioo_ctlg_categories', function(Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('name');
        });

        Schema::create('iioo_ctlg_subcategories', function(Blueprint $table){
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('name');
            $table->integer('category_id')->unsigned();

            $table->foreign('category_id', 'iioo_ctlg_subcategory_category_foreign')->references('id')->on('iioo_ctlg_categories')->onDelete('cascade');
        });

        Schema::create('iioo_ctlg_brands', function(Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('name');
            $table->boolean('display_on_home')->default(false);
        });

        Schema::create('iioo_ctlg_products', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->text('description')->nullable();
            $table->integer('subcategory_id')->unsigned();
            $table->integer('brand_id')->unsigned();
            $table->float('price');

            $table->foreign('subcategory_id', 'iioo_ctlg_products_subcategory_foreign')->references('id')->on('iioo_ctlg_subcategories')->onDelete('restrict');
            $table->foreign('brand_id', 'iioo_ctlg_products_brand_foreign')->references('id')->on('iioo_ctlg_brands')->onDelete('restrict');
        });

        Schema::create('iioo_ctlg_information_fields', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('iioo_ctlg_subcategory_information', function(Blueprint $table){
            $table->integer('subcategory_id')->unsigned();
            $table->integer('information_field_id')->unsigned();
            $table->integer('position')->default(0);
            $table->primary(['subcategory_id', 'information_field_id'], 'iioo_ctlg_subcategory_information_field_primary');

            $table->foreign('subcategory_id', 'iioo_ctlg_subcategories_information_fields_foreign')->references('id')->on('iioo_ctlg_subcategories')->onDelete('cascade');
            $table->foreign('information_field_id', 'iioo_ctlg_information_fields_subcategories_foreign')->references('id')->on('iioo_ctlg_information_fields')->onDelete('cascade');
        });

        Schema::create('iioo_ctlg_product_information', function(Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->integer('information_field_id')->unsigned();
            $table->string('value');
            $table->primary(['product_id', 'information_field_id'], 'iioo_ctlg_product_information_field_primary');

            $table->foreign('product_id', 'iioo_ctlg_products_information_fields_foreign')->references('id')->on('iioo_ctlg_products')->onDelete('cascade');
            $table->foreign('information_field_id', 'iioo_ctlg_information_fields_products_foreign')->references('id')->on('iioo_ctlg_information_fields')->onDelete('restrict');
        });

        Schema::create('iioo_ctlg_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('iioo_ctlg_order_product', function(Blueprint $table) {
            $table->integer('order_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->primary(['order_id', 'product_id'], 'iioo_ctlg_order_product_primary');

            $table->foreign('order_id', 'iioo_ctlg_order_product_foreign')->references('id')->on('iioo_ctlg_orders')->onDelete('cascade');
            $table->foreign('product_id', 'iioo_ctlg_product_order_foreign')->references('id')->on('iioo_ctlg_products')->onDelete('restrict');
        });
    }

    public function down()
    {
        Schema::dropIfExists('iioo_ctlg_order_product');
        Schema::dropIfExists('iioo_ctlg_orders');
        Schema::dropIfExists('iioo_ctlg_product_information');
        Schema::dropIfExists('iioo_ctlg_subcategory_information');
        Schema::dropIfExists('iioo_ctlg_information_fields');
        Schema::dropIfExists('iioo_ctlg_products');
        Schema::dropIfExists('iioo_ctlg_brands');
        Schema::dropIfExists('iioo_ctlg_subcategories');
        Schema::dropIfExists('iioo_ctlg_categories');
    }
}