<?php
namespace IIOO\Catalogue\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddCurrencyToProductsTable extends Migration
{
    public function up()
    {
        Schema::table('iioo_ctlg_products', function (Blueprint $table) {
            $table->enum('currency', ['UAH', 'USD', 'EUR'])->default('USD')->after('price');
        });
    }

    public function down()
    {
        Schema::table('iioo_ctlg_products', function (Blueprint $table) {
            $table->dropColumn('currency');
        });
    }
}
?>