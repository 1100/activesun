<?php
namespace IIOO\Catalogue\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddCredentialsToOrderMigration extends Migration
{
    public function up()
    {
        Schema::table('iioo_ctlg_orders', function (Blueprint $table) {
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone');
            $table->string('email')->nullable();
        });
    }

    public function down()
    {
        Schema::table('iioo_ctlg_orders', function (Blueprint $table) {
            $table->dropColumn(['first_name', 'last_name', 'phone', 'email']);
        });
    }
}
?>