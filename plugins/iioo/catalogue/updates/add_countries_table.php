<?php
namespace IIOO\Catalogue\Updates;

use Schema;
use DB;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddCountriesMigration extends Migration
{
    public function up()
    {
        Schema::create('iioo_ctlg_countries', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('code')->unique();
        });

        Schema::table('iioo_ctlg_products', function(Blueprint $table) {
            $table->integer('country_id')->unsigned()->nullable();
            $table->foreign('country_id', 'iioo_ctlg_products_country_foreign')->references('id')->on('iioo_ctlg_countries')->onDelete('restrict');
        });

        DB::table('iioo_ctlg_countries')->insert([
            [
                'name' => 'Австралия',
                'code' => 'AU',
            ],[
                'name' => 'Австрия',
                'code' => 'AT',
            ],[
                'name' => 'Азербайджан',
                'code' => 'AZ',
            ],[
                'name' => 'Албания',
                'code' => 'AL',
            ],[
                'name' => 'Алжир',
                'code' => 'DZ',
            ],[
                'name' => 'Ангола',
                'code' => 'AO',
            ],[
                'name' => 'Андорра',
                'code' => 'AD',
            ],[
                'name' => 'Антигуа и Барбуда',
                'code' => 'AG',
            ],[
                'name' => 'Аргентина',
                'code' => 'AR',
            ],[
                'name' => 'Армения',
                'code' => 'AM',
            ],[
                'name' => 'Афганистан',
                'code' => 'AF',
            ],[
                'name' => 'Багамы',
                'code' => 'BS',
            ],[
                'name' => 'Бангладеш',
                'code' => 'BD',
            ],[
                'name' => 'Барбадос',
                'code' => 'BB',
            ],[
                'name' => 'Бахрейн',
                'code' => 'BH',
            ],[
                'name' => 'Беларусь',
                'code' => 'BY',
            ],[
                'name' => 'Белиз',
                'code' => 'BZ',
            ],[
                'name' => 'Бельгия',
                'code' => 'BE',
            ],[
                'name' => 'Бенин',
                'code' => 'BJ',
            ],[
                'name' => 'Бермуды',
                'code' => 'BM',
            ],[
                'name' => 'Болгария',
                'code' => 'BG',
            ],[
                'name' => 'Боливия',
                'code' => 'BO',
            ],[
                'name' => 'Босния и Герцеговина',
                'code' => 'BA',
            ],[
                'name' => 'Ботсвана',
                'code' => 'BW',
            ],[
                'name' => 'Бразилия',
                'code' => 'BR',
            ],[
                'name' => 'Бруней-Даруссалам',
                'code' => 'BN',
            ],[
                'name' => 'Буркина-Фасо',
                'code' => 'BF',
            ],[
                'name' => 'Бурунди',
                'code' => 'BI',
            ],[
                'name' => 'Бутан',
                'code' => 'BT',
            ],[
                'name' => 'Вануату',
                'code' => 'VU',
            ],[
                'name' => 'Ватикан',
                'code' => 'VA',
            ],[
                'name' => 'Венгрия',
                'code' => 'HU',
            ],[
                'name' => 'Венесуэла',
                'code' => 'VE',
            ],[
                'name' => 'Вьетнам',
                'code' => 'VN',
            ],[
                'name' => 'Габон',
                'code' => 'GA',
            ],[
                'name' => 'Гаити',
                'code' => 'HT',
            ],[
                'name' => 'Гайана',
                'code' => 'GY',
            ],[
                'name' => 'Гамбия',
                'code' => 'GM',
            ],[
                'name' => 'Гана',
                'code' => 'GH',
            ],[
                'name' => 'Гватемала',
                'code' => 'GT',
            ],[
                'name' => 'Гвинея',
                'code' => 'GN',
            ],[
                'name' => 'Гвинея-Бисау',
                'code' => 'GW',
            ],[
                'name' => 'Германия',
                'code' => 'DE',
            ],[
                'name' => 'Гондурас',
                'code' => 'HN',
            ],[
                'name' => 'Гренада',
                'code' => 'GD',
            ],[
                'name' => 'Греция',
                'code' => 'GR',
            ],[
                'name' => 'Грузия',
                'code' => 'GE',
            ],[
                'name' => 'Дания',
                'code' => 'DK',
            ],[
                'name' => 'Джибути',
                'code' => 'DJ',
            ],[
                'name' => 'Доминика',
                'code' => 'DM',
            ],[
                'name' => 'Доминиканская Республика',
                'code' => 'DO',
            ],[
                'name' => 'Египет',
                'code' => 'EG',
            ],[
                'name' => 'Замбия',
                'code' => 'ZM',
            ],[
                'name' => 'Западная Сахара',
                'code' => 'EH',
            ],[
                'name' => 'Зимбабве',
                'code' => 'ZW',
            ],[
                'name' => 'Израиль',
                'code' => 'IL',
            ],[
                'name' => 'Индия',
                'code' => 'IN',
            ],[
                'name' => 'Индонезия',
                'code' => 'ID',
            ],[
                'name' => 'Иордания',
                'code' => 'JO',
            ],[
                'name' => 'Ирак',
                'code' => 'IQ',
            ],[
                'name' => 'Иран',
                'code' => 'IR',
            ],[
                'name' => 'Ирландия',
                'code' => 'IE',
            ],[
                'name' => 'Исландия',
                'code' => 'IS',
            ],[
                'name' => 'Испания',
                'code' => 'ES',
            ],[
                'name' => 'Италия',
                'code' => 'IT',
            ],[
                'name' => 'Йемен',
                'code' => 'YE',
            ],[
                'name' => 'Кабо-Верде',
                'code' => 'CV',
            ],[
                'name' => 'Казахстан',
                'code' => 'KZ',
            ],[
                'name' => 'Камбоджа',
                'code' => 'KH',
            ],[
                'name' => 'Камерун',
                'code' => 'CM',
            ],[
                'name' => 'Канада',
                'code' => 'CA',
            ],[
                'name' => 'Катар',
                'code' => 'QA',
            ],[
                'name' => 'Кения',
                'code' => 'KE',
            ],[
                'name' => 'Кипр',
                'code' => 'CY',
            ],[
                'name' => 'Киргизия',
                'code' => 'KG',
            ],[
                'name' => 'Кирибати',
                'code' => 'KI',
            ],[
                'name' => 'Китай',
                'code' => 'CN',
            ],[
                'name' => 'Колумбия',
                'code' => 'CO',
            ],[
                'name' => 'Коморы',
                'code' => 'KM',
            ],[
                'name' => 'Конго',
                'code' => 'CG',
            ],[
                'name' => 'Конго, Демократическая Республика',
                'code' => 'CD',
            ],[
                'name' => 'Корея, Народно-Демократическая Республика',
                'code' => 'KP',
            ],[
                'name' => 'Корея',
                'code' => 'KR',
            ],[
                'name' => 'Коста-Рика',
                'code' => 'CR',
            ],[
                'name' => 'Кот д\'Ивуар',
                'code' => 'CI',
            ],[
                'name' => 'Куба',
                'code' => 'CU',
            ],[
                'name' => 'Кувейт',
                'code' => 'KW',
            ],[
                'name' => 'Лаос',
                'code' => 'LA',
            ],[
                'name' => 'Латвия',
                'code' => 'LV',
            ],[
                'name' => 'Лесото',
                'code' => 'LS',
            ],[
                'name' => 'Ливан',
                'code' => 'LB',
            ],[
                'name' => 'Ливия',
                'code' => 'LY',
            ],[
                'name' => 'Либерия',
                'code' => 'LR',
            ],[
                'name' => 'Лихтенштейн',
                'code' => 'LI',
            ],[
                'name' => 'Литва',
                'code' => 'LT',
            ],[
                'name' => 'Люксембург',
                'code' => 'LU',
            ],[
                'name' => 'Маврикий',
                'code' => 'MU',
            ],[
                'name' => 'Мавритания',
                'code' => 'MR',
            ],[
                'name' => 'Мадагаскар',
                'code' => 'MG',
            ],[
                'name' => 'Малави',
                'code' => 'MW',
            ],[
                'name' => 'Малайзия',
                'code' => 'MY',
            ],[
                'name' => 'Мали',
                'code' => 'ML',
            ],[
                'name' => 'Мальдивы',
                'code' => 'MV',
            ],[
                'name' => 'Мальта',
                'code' => 'MT',
            ],[
                'name' => 'Марокко',
                'code' => 'MA',
            ],[
                'name' => 'Маршалловы острова',
                'code' => 'MH',
            ],[
                'name' => 'Мексика',
                'code' => 'MX',
            ],[
                'name' => 'Микронезия',
                'code' => 'FM',
            ],[
                'name' => 'Мозамбик',
                'code' => 'MZ',
            ],[
                'name' => 'Молдова',
                'code' => 'MD',
            ],[
                'name' => 'Монако',
                'code' => 'MC',
            ],[
                'name' => 'Монголия',
                'code' => 'MN',
            ],[
                'name' => 'Мьянма',
                'code' => 'MM',
            ],[
                'name' => 'Намибия',
                'code' => 'NA',
            ],[
                'name' => 'Науру',
                'code' => 'NR',
            ],[
                'name' => 'Непал',
                'code' => 'NP',
            ],[
                'name' => 'Нигер',
                'code' => 'NE',
            ],[
                'name' => 'Нигерия',
                'code' => 'NG',
            ],[
                'name' => 'Нидерланды',
                'code' => 'NL',
            ],[
                'name' => 'Никарагуа',
                'code' => 'NI',
            ],[
                'name' => 'Новая Зеландия',
                'code' => 'NZ',
            ],[
                'name' => 'Норвегия',
                'code' => 'NO',
            ],[
                'name' => 'Объединенные Арабские Эмираты',
                'code' => 'AE',
            ],[
                'name' => 'Оман',
                'code' => 'OM',
            ],[
                'name' => 'Пакистан',
                'code' => 'PK',
            ],[
                'name' => 'Палау',
                'code' => 'PW',
            ],[
                'name' => 'Панама',
                'code' => 'PA',
            ],[
                'name' => 'Папуа-Новая Гвинея',
                'code' => 'PG',
            ],[
                'name' => 'Парагвай',
                'code' => 'PY',
            ],[
                'name' => 'Перу',
                'code' => 'PE',
            ],[
                'name' => 'Польша',
                'code' => 'PL',
            ],[
                'name' => 'Португалия',
                'code' => 'PT',
            ],[
                'name' => 'Республика Македония',
                'code' => 'MK',
            ],[
                'name' => 'Россия',
                'code' => 'RU',
            ],[
                'name' => 'Руанда',
                'code' => 'RW',
            ],[
                'name' => 'Румыния',
                'code' => 'RO',
            ],[
                'name' => 'Самоа',
                'code' => 'WS',
            ],[
                'name' => 'Сан-Марино',
                'code' => 'SM',
            ],[
                'name' => 'Сан-Томе и Принсипи',
                'code' => 'ST',
            ],[
                'name' => 'Саудовская Аравия',
                'code' => 'SA',
            ],[
                'name' => 'Свазиленд',
                'code' => 'SZ',
            ],[
                'name' => 'Сенегал',
                'code' => 'SN',
            ],[
                'name' => 'Сент-Винсент и Гренадины',
                'code' => 'VC',
            ],[
                'name' => 'Сент-Китс и Невис',
                'code' => 'KN',
            ],[
                'name' => 'Сент-Люсия',
                'code' => 'LC',
            ],[
                'name' => 'Сербия',
                'code' => 'RS',
            ],[
                'name' => 'Сейшелы',
                'code' => 'SC',
            ],[
                'name' => 'Сингапур',
                'code' => 'SG',
            ],[
                'name' => 'Сирийская Арабская Республика',
                'code' => 'SY',
            ],[
                'name' => 'Словакия',
                'code' => 'SK',
            ],[
                'name' => 'Словения',
                'code' => 'SI',
            ],[
                'name' => 'Великобритания',
                'code' => 'GB',
            ],[
                'name' => 'Соединенные Штаты (США)',
                'code' => 'US',
            ],[
                'name' => 'Соломоновы острова',
                'code' => 'SB',
            ],[
                'name' => 'Сомали',
                'code' => 'SO',
            ],[
                'name' => 'Судан',
                'code' => 'SD',
            ],[
                'name' => 'Суринам',
                'code' => 'SR',
            ],[
                'name' => 'Сьерра-Леоне',
                'code' => 'SL',
            ],[
                'name' => 'Таджикистан',
                'code' => 'TJ',
            ],[
                'name' => 'Таиланд',
                'code' => 'TH',
            ],[
                'name' => 'Тайвань',
                'code' => 'TW',
            ],[
                'name' => 'Танзания',
                'code' => 'TZ',
            ],[
                'name' => 'Тимор-Лесте',
                'code' => 'TL',
            ],[
                'name' => 'Того',
                'code' => 'TG',
            ],[
                'name' => 'Тринидад и Тобаго',
                'code' => 'TT',
            ],[
                'name' => 'Тунис',
                'code' => 'TN',
            ],[
                'name' => 'Туркмения',
                'code' => 'TM',
            ],[
                'name' => 'Турция',
                'code' => 'TR',
            ],[
                'name' => 'Уганда',
                'code' => 'UG',
            ],[
                'name' => 'Узбекистан',
                'code' => 'UZ',
            ],[
                'name' => 'Украина',
                'code' => 'UA',
            ],[
                'name' => 'Уругвай',
                'code' => 'UY',
            ],[
                'name' => 'Фиджи',
                'code' => 'FJ',
            ],[
                'name' => 'Филиппины',
                'code' => 'PH',
            ],[
                'name' => 'Финляндия',
                'code' => 'FI',
            ],[
                'name' => 'Франция',
                'code' => 'FR',
            ],[
                'name' => 'Хорватия',
                'code' => 'HR',
            ],[
                'name' => 'Центрально-Африканская Республика',
                'code' => 'CF',
            ],[
                'name' => 'Чад',
                'code' => 'TD',
            ],[
                'name' => 'Черногория',
                'code' => 'ME',
            ],[
                'name' => 'Чешская Республика',
                'code' => 'CZ',
            ],[
                'name' => 'Чили',
                'code' => 'CL',
            ],[
                'name' => 'Швейцария',
                'code' => 'CH',
            ],[
                'name' => 'Швеция',
                'code' => 'SE',
            ],[
                'name' => 'Шри-Ланка',
                'code' => 'LK',
            ],[
                'name' => 'Эквадор',
                'code' => 'EC',
            ],[
                'name' => 'Экваториальная Гвинея',
                'code' => 'GQ',
            ],[
                'name' => 'Эль-Сальвадор',
                'code' => 'SV',
            ],[
                'name' => 'Эритрея',
                'code' => 'ER',
            ],[
                'name' => 'Эстония',
                'code' => 'EE',
            ],[
                'name' => 'Эфиопия',
                'code' => 'ET',
            ],[
                'name' => 'Южная Африка',
                'code' => 'ZA',
            ],[
                'name' => 'Ямайка',
                'code' => 'JM',
            ],[
                'name' => 'Япония',
                'code' => 'JP',
            ]
        ]);
    }

    public function down()
    {
        Schema::table('iioo_ctlg_products', function (Blueprint $table) {
            $table->dropForeign('iioo_ctlg_products_country_foreign');
            $table->dropColumn('country_id');
        });
        Schema::dropIfExists('iioo_ctlg_countries');
    }
}