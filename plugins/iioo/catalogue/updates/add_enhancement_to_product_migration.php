<?php
namespace IIOO\Catalogue\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddProductsEnhancementMigration extends Migration
{
    public function up()
    {
        Schema::table('iioo_ctlg_products', function (Blueprint $table) {
            $table->boolean('is_available')->default(true);
            $table->string('code')->nullable();
            $table->string('keywords')->nullable();
        });
    }

    public function down()
    {
        Schema::table('iioo_ctlg_products', function (Blueprint $table) {
            $table->dropColumn('is_available');
            $table->dropColumn('code');
            $table->dropColumn('keywords');
        });
    }
}
?>