<?php
namespace IIOO\Catalogue\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateProductsCountMigration extends Migration
{
    public function up()
    {
        Schema::table('iioo_ctlg_products', function (Blueprint $table) {
            $table->string('code')->unique()->change();
        });
    }

    public function down()
    {
        Schema::table('iioo_ctlg_products', function (Blueprint $table) {
            $table->string('code')->nullable()->change();
        });
    }
}
?>