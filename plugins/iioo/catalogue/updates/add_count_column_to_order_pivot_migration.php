<?php
namespace IIOO\Catalogue\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddProductsCountColumnToOrderPivotMigration extends Migration
{
    public function up()
    {
        Schema::table('iioo_ctlg_order_product', function (Blueprint $table) {
            $table->string('products_count');
        });
    }

    public function down()
    {
        Schema::table('iioo_ctlg_order_product', function (Blueprint $table) {
            $table->dropColumn('products_count');
        });
    }
}
?>