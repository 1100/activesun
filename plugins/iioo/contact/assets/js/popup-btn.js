jQuery('.popup-with-move-anim').magnificPopup({
    type: 'inline',
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom'
});
jQuery(window).on('ajaxBeforeSend', function() {
    jQuery('#send-contact-btn').prop('disabled',true);
});
jQuery(window).on('ajaxUpdateComplete', function() {
    jQuery('#send-contact-btn').prop('disabled',false);
});
jQuery('#send-contact').on('ajaxSuccess', function() {
    document.getElementById('send-contact').reset();
    console.log('success');
});
jQuery('#send-contact').on('ajaxFail', function() {
    console.log('fail');
});