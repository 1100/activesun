<?php namespace IIOO\Contact;

use Backend;
use System\Classes\PluginBase;

/**
 * Contact Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'iioo.contact::lang.plugin.name',
            'description' => 'iioo.contact::lang.plugin.description',
            'author'      => 'Misha Cheipesh, Roman Movchan',
            'icon'        => 'icon-volume-control-phone'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'IIOO\Contact\Components\ContactForm' => 'ContactForm',
            'IIOO\Contact\Components\ContactButton' => 'ContactButton'
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'iioo.contact.access_contact' => [
                'tab'   => 'iioo.contact::lang.permissions.tab',
                'label' => 'iioo.contact::lang.permissions.manage',
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'contact' => [
                'permissions' => ['iioo.contact.access_contact'],
                'label' => 'iioo.contact::lang.navigation.label',
                'url' => Backend::url('iioo/contact/messages'),
                'icon' => 'icon-envelope-o',
                'order' => 500,
                'sideMenu' => [
                    'messages' => [
                        'permissions' => ['iioo.contact.access_contact'],
                        'label' => 'iioo.contact::lang.navigation.messages.label',
                        'url' => Backend::url('iioo/contact/messages'),
                        'icon' => 'icon-envelope-o',
                    ],
                    'consultation' => [
                        'permissions' => ['iioo.contact.access_contact'],
                        'label' => 'iioo.contact::lang.navigation.consultations.label',
                        'url' => Backend::url('iioo/contact/consultations'),
                        'icon' => 'icon-volume-control-phone',
                    ]
                ]
            ]
        ];
    }

    /**
     * Registers back-end settings for this plugin.
     *
     * @return array
     */
    public function registerSettings()
    {
        return [
            'config' => [
                'label'       => 'iioo.contact::lang.settings.name',
                'icon'        => 'icon-envelope-o',
                'description' => 'iioo.contact::lang.settings.description',
                'class'       => 'IIOO\Contact\Models\Settings',
                'permissions' => ['iioo.contact.access_contact'],
                'order'       => 60
            ]
        ];
    }
}
