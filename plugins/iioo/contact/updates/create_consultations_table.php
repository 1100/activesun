<?php namespace IIOO\Contact\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateConsultationsTable extends Migration
{
    public function up()
    {
        Schema::create('iioo_contact_consultations', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('customer_name');
            $table->string('customer_phone');
            $table->text('message')->nullable();
            $table->string('referer_name')->nullable();
            $table->string('referer_url')->nullable();
            $table->boolean('is_readed')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('iioo_contact_consultations');
    }
}
