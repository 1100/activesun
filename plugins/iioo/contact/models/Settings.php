<?php namespace IIOO\Contact\Models;

use Model;

/**
 * Settings Model
 */
class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];
    public $settingsCode = 'iioo_contact_settings';
    public $settingsFields = 'fields.yaml';
}
