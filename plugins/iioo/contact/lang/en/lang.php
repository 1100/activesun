<?php
return [
    'plugin' => [
        'name' => 'Contact Forms',
        'description' => 'Plugin IIOO.Contact',
    ],
    'permissions' => [
        'tab' => 'Contact Forms',
        'manage' => 'Manage contact forms'
    ],
    'columns' => [
        'message' => [
            'id' => 'ID',
            'created_at' => 'Created at',
            'customer_name' => 'Name',
            'customer_phone' => 'Phone',
            'customer_email' => 'Email',
            'message' => 'Message',
            'referer_name' => 'Page name',
            'referer_url' => 'Page url',
            'readed' => 'Is read',
        ],
    ],
    'fields' => [
        'message' => [
            'created_at' => 'Created at',
            'customer_name' => 'Name',
            'customer_phone' => 'Phone',
            'customer_email' => 'Email',
            'message' => 'Message',
            'referer_name' => 'Page name',
            'referer_url' => 'Page url',
            'readed' => 'Is readed',
        ],
    ],
    'settings' => [
        'name' => 'Contact Forms',
        'description' => 'Email settings',
        'notification_email_address' => 'Notification Email Address',
        'notification_email_address_comment' => 'Can be separated by commas',
    ],
    'components' => [
        'validation' => [
            'phone' => [
                'required' => 'Phone field is required',
                'required_without' => 'One of these is required: phone, email',
            ],
            'name' => [
                'required' => 'Name field is required',
            ],
            'email' => [
                'required' => 'Email field is required',
                'required_without' => 'One of these is required: phone, email',
                'email' => 'Not valid email format',
            ],
            'message' => [
                'required' => 'Message field is required',
            ]
        ],
        'flash' => [
            'success_call' => 'Thank you for contacting us. We\'ll call you soon.',
            'success_contact' => 'Thank you for contacting us',
        ],
    ],
];