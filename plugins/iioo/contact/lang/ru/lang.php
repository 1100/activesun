<?php
return [
    'plugin' => [
        'name' => 'Контактные формы',
        'description' => 'Плагин IIOO.Contact_Forms',
    ],
    'permissions' => [
        'tab' => 'Контактные формы',
        'manage' => 'Управление контактными формами'
    ],
    'navigation' => [
        'label' => 'Сообщения',
        'messages' => [
            'label' => 'Сообщения'
        ],
        'consultations' => [
            'label' => 'Консультации'
        ]
    ],
    'controller' => [
        'list' => [
            'messages' => 'Сообщения',
            'consultations' => 'Консультации',
        ],
        'view' => [
            'messages' => [
                'breadcrumb_label' => 'Все сообщения',
            ],
            'consultations' => [
                'breadcrumb_label' => 'Все запросы консультации',
            ]
        ],
        'form' => [
            'message' => [
                'read' => 'Сообщение',
            ],
            'consultation' => [
                'read' => 'Запрос',
            ]
        ],
    ],
    'columns' => [
        'message' => [
            'id' => 'ID',
            'created_at' => 'Дата',
            'customer_name' => 'Имя',
            'customer_phone' => 'Телефон',
            'customer_email' => 'Email',
            'message' => 'Сообщение',
            'referer_name' => 'Страница',
            'referer_url' => 'Ссылка на страницу',
            'readed' => 'Прочитано',
        ],
        'consultation' => [
            'id' => 'ID',
            'created_at' => 'Дата',
            'customer_name' => 'Имя',
            'customer_phone' => 'Телефон',
            'referer_name' => 'Страница',
            'referer_url' => 'Ссылка на страницу',
            'readed' => 'Прочитано',
        ],
    ],
    'fields' => [
        'message' => [
            'created_at' => 'Дата',
            'customer_name' => 'Имя',
            'customer_phone' => 'Телефон',
            'customer_email' => 'Email',
            'message' => 'Сообщение',
            'referer_name' => 'Страница',
            'referer_url' => 'Ссылка на страницу',
            'readed' => 'Прочитано',
        ],
        'consultations' => [
            'created_at' => 'Дата',
            'customer_name' => 'Имя',
            'customer_phone' => 'Телефон',
            'referer_name' => 'Страница',
            'referer_url' => 'Ссылка на страницу',
            'readed' => 'Прочитано',
        ],
    ],
    'settings' => [
        'name' => 'Контактные формы',
        'description' => 'Настройки email',
        'notification_email_address' => 'Email для уведомлений',
        'notification_email_address_comment' => 'Можно несколько через запятую',
    ],
    'components' => [
        'validation' => [
            'phone' => [
                'required' => 'Пожалуйста, укажите Ваш телефон',
                'required_without' => 'Пожалуйста, укажите телефон и/или email',
            ],
            'name' => [
                'required' => 'Пожалуйста, укажите Ваше имя',
            ],
            'email' => [
                'required' => 'Пожалуйста, укажите Ваш email',
                'required_without' => 'Пожалуйста, укажите телефон и/или email',
                'email' => 'Неправильный формат email',
            ],
            'message' => [
                'required' => 'Сообщение не может быть пустым',
            ]
        ],
        'flash' => [
            'success_contact' => 'Спасибо за Ваше обращение.',
            'success_consultation' => 'Вы успешно заказали консультацию. Мы свяжемся с Вами в ближайшее время.',
        ],
    ],
];