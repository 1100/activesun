<?php namespace IIOO\Contact\Components;

use Cms\Classes\ComponentBase;
use Validator;
use ValidationException;
use AjaxException;
use October\Rain\Support\Facades\Flash;
use IIOO\Contact\Models\Consultation;
use IIOO\Contact\Models\Settings;
use Illuminate\Support\Facades\URL;
use Mail;

class ContactButton extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'CallPopupBtn Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun()
    {
        $this->addJs('/plugins/iioo/contact/assets/js/popup-btn.js');
        $this->addCss('/plugins/iioo/contact/assets/css/popup-btn.css');
        $this->addJs('/plugins/iioo/contact/assets/js/jquery.maskedinput.min.js');
        $this->addJs('/plugins/iioo/contact/assets/js/phone-field-mask.js');
    }

    public function onSendPopupContact()
    {
        /**
         * Form validation
         */
        $customValidationMessages = [
            'contact_btn_name.required' => trans('iioo.contact::lang.components.validation.name.required'),
            'contact_btn_phone.required' => trans('iioo.contact::lang.components.validation.phone.required'),
        ];
        $formValidationRules = [
            'contact_btn_name' => 'required',
            'contact_btn_phone' => 'required|regex:/([0-9 \-+().])*/',
        ];

        $validation = Validator::make(post(), $formValidationRules, $customValidationMessages);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        } else {

            $message = new Consultation();
            $message->customer_name = post('contact_btn_name');
            $message->customer_phone = post('contact_btn_phone');
            $message->referer_url = URL::current();
            $message->referer_name = $this->page->title;
            $message->save();

            if( !empty(Settings::get('notification_email_address', '')) ) {
                $this->sendNotificationMail($message);
            }

            return ['#send-contact' => $this->renderPartial('@form_content.htm', [
                'success_message' => trans('iioo.contact::lang.components.flash.success_consultation')
            ])];
        }
    }

    protected function sendNotificationMail($message)
    {
        $vars = [
            'customer_name' => $message->customer_name,
            'customer_phone' => $message->customer_phone,
            'referer_url' => $message->referer_url,
            'referer_name' => $message->referer_name
        ];

        $recipients = array_map('trim', explode(',', Settings::get('notification_email_address')));

        Mail::sendTo($recipients, 'iioo.contact::mail.notification_consultation', $vars);
    }
}