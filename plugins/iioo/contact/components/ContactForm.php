<?php namespace IIOO\Contact\Components;

use Cms\Classes\ComponentBase;
use Validator;
use ValidationException;
use October\Rain\Support\Facades\Flash;
use IIOO\Contact\Models\Message;
use IIOO\Contact\Models\Settings;
use Illuminate\Support\Facades\URL;
use Mail;

class ContactForm extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'ContactForm Component',
            'description' => ''
        ];
    }

    public function onRun()
    {
        $this->addJs('/plugins/iioo/contact/assets/js/jquery.maskedinput.min.js');
        $this->addJs('/plugins/iioo/contact/assets/js/phone-field-mask.js');
    }

    public function onSendContact()
    {
        /**
         * Form validation
         */
        $customValidationMessages = [
            'contact_form_name.required' => trans('iioo.contact::lang.components.validation.name.required'),
            'contact_form_email.required_without' => trans('iioo.contact::lang.components.validation.email.required_without'),
            'contact_form_email.email' => trans('iioo.contact::lang.components.validation.email.email'),
            'contact_form_phone.required_without' => trans('iioo.contact::lang.components.validation.phone.required_without'),
            'contact_form_message.required' => trans('iioo.contact::lang.components.validation.message.required'),
        ];
        $formValidationRules = [
            'contact_form_name' => 'required',
            'contact_form_email' => 'required_without:contact_form_phone|email',
            'contact_form_phone' => 'required_without:contact_form_email|regex:/([0-9 \-+().])*/',
            'contact_form_message' => 'required',
        ];

        $validation = Validator::make(post(), $formValidationRules, $customValidationMessages);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        } else {

            $message = new Message;
            $message->customer_name = post('contact_form_name');
            $message->customer_phone = post('contact_form_phone');
            $message->customer_email = post('contact_form_email');
            $message->message = post('contact_form_message');
            $message->referer_url = URL::current();
            $message->referer_name = $this->page->title;
            $message->save();

            if( !empty(Settings::get('notification_email_address', '')) ) {
                $this->sendNotificationMail($message);
            }

            return ['#send-contact-form' => $this->renderPartial('@form_content.htm', [
                'success_message' => trans('iioo.contact::lang.components.flash.success_contact')
            ])];

        }
    }

    protected function sendNotificationMail($message)
    {
        $vars = [
            'customer_name' => $message->customer_name,
            'customer_phone' => $message->customer_phone,
            'customer_email' => $message->customer_email,
            'customer_message' => $message->message
        ];

        $recipients = array_map('trim', explode(',', Settings::get('notification_email_address')));

        Mail::sendTo($recipients, 'iioo.contact::mail.notification_message', $vars);
    }
}
