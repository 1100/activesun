window.v_ids = ["video-roof", "video-solar"];
if (matchMedia) {
    var mq = window.matchMedia("(min-width: 1000px)");
    mq.addListener(checkWindowWidthForVideo);
    checkWindowWidthForVideo(mq);
}

function checkWindowWidthForVideo(mq) {
    if (mq.matches) {
        window.current_v_index = 0;
        var current = document.getElementById(window.v_ids[window.current_v_index]);
        showVideo(current);
    } else {
        for (var i=0; i<window.v_ids.length; i++) {
            var video_el =document.getElementById(window.v_ids[i]);
            video_el.pause();
            video_el.classList.remove("visible_video");
            video_el.classList.add("hidden_video");
        }
    }
}

function nextVideoIndex() {
    return window.current_v_index + 1 >= window.v_ids.length ? 0 : window.current_v_index + 1;
}

function showVideo(el) {
    el.classList.remove("hidden_video");
    el.classList.add("visible_video");
    el.play();
    el.addEventListener('ended', onVideoEnd);
}

function onVideoEnd(e) {
    current = e.target;
    next_id = window.v_ids[nextVideoIndex()];
    next = document.getElementById(next_id);
    current.classList.remove("visible_video");
    current.classList.add("hidden_video");
    showVideo(next);
    window.current_v_index = nextVideoIndex();
}