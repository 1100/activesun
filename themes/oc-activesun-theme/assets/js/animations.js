$(document).ready(function(){
    $('[data-anim]').each(function () {
        var $this = $(this);
        $this.css('opacity', 0);
        $this.waypoint( function () {
            $this.css('opacity', 1);
            $this.addClass('animated').addClass($this.attr('data-anim'));
        },{ triggerOnce: true, offset: '90%' });
    });
});