$(".categories_spoiler").smk_Accordion({
    showIcon: true,
    animation: true,
    closeAble: true,
    slideSpeed: 300
});

if (matchMedia) {
    var categories_spoiler_media_query = window.matchMedia("(min-width: 1000px)");
    categories_spoiler_media_query.addListener(checkWindowWidthForCategoriesSpoiler);
    checkWindowWidthForCategoriesSpoiler(categories_spoiler_media_query);
}

function checkWindowWidthForCategoriesSpoiler(mq) {
    if (mq.matches) {
        if(!$(".categories_spoiler .accordion_in").hasClass('acc_active')) {
            $('.categories_spoiler .acc_head').click();
        }
    } else {
        if($(".categories_spoiler .accordion_in").hasClass('acc_active')) {
            $('.categories_spoiler .acc_head').click();
        }
    }
}